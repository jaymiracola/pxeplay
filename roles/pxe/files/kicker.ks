#platform=x86, AMD64, or Intel EM64T
#version=DEVEL
# Keyboard layouts
keyboard 'us'
# Root password
rootpw --iscrypted $1$N/vmpWaC$QUgg.p8bWMdL60jOcLMBH0
# Add config management user
user --name=ansible --groups=wheel --iscrypted --password=$1$N/vmpWaC$QUgg.p8bWMdL60jOcLMBH0
# System language
lang en_US
# Use network installation
url --url="http://192.168.12.1/rhel6.9"
# Reboot after installation
reboot
# System timezone
timezone America/New_York
# Use graphical install
graphical
# Network information
network  --bootproto=static --gateway=192.168.12.1 --ip=192.168.12.220 --netmask=255.255.255.0
# System authorization information
auth  --useshadow  --passalgo=sha512
# Firewall configuration
firewall --enabled --port=ssh
# SELinux configuration
selinux --enforcing
# System bootloader configuration
bootloader --location=mbr
# Partition clearing information
clearpart --all
# Automatically partition
autopart

%packages
@core

%end

